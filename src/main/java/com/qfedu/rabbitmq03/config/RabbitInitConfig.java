package com.qfedu.rabbitmq03.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/3 16:31
 */
@Configuration
public class RabbitInitConfig {

    /**
     * 1.创建队列
     * 生成死信 ：1.有效期 2.设置死信交换器*/
    @Bean
    public Queue createQ1(){
        Map<String,Object> map=new HashMap<>();
        //设置 队列中消息的有效期 单位 毫秒
        map.put("x-message-ttl",15000);
        //设置 对应的死信交换器的名称
        map.put("x-dead-letter-exchange","dead-exchange");
        //设置 死信消息的路由关键字
        map.put("x-dead-letter-routing-key","first");
        //创建消息队列
        return QueueBuilder.durable("lx-dead-ttl").withArguments(map).build();
    }
    //接收死信消息
    @Bean
    public Queue createQ2(){
        return new Queue("lx-dead-msg");
    }
    /**
     * 2.创建交换器
     * 死信交换 路由模式 */
    @Bean
    public DirectExchange createDe(){
        return new DirectExchange("dead-exchange");
    }
    /**
     * 3.绑定 实现消息从交换器-->队列*/
    @Bean
    public Binding createB1(DirectExchange de){
        return BindingBuilder.bind(createQ2()).to(de).with("first");
    }
}
