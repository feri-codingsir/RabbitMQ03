package com.qfedu.rabbitmq03.controller;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * ━━━━━━Feri出没━━━━━━
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　 ┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　  ┃
 * 　　┃　　　　　　 ┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┃    邢哥的代码，怎么会，有bug呢，不可能！
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━永无BUG!━━━━━━
 *
 * @Description:
 * @Author：邢朋辉
 * @Date: 2023/8/3 16:39
 */
@RestController
@RequestMapping("/api/mq/")
public class DeadMsgController {
    @Resource
    private RabbitTemplate template;
    //队列的有效期
    @GetMapping("send/{msg}")
    public String send(@PathVariable String msg){
        template.convertAndSend("","lx-dead-ttl",msg+"-"+System.currentTimeMillis());
        return "OK";
    }
    //消息的有效期
    @GetMapping("send2/{msg}")
    public String send2(@PathVariable String msg){
        //消息对象
        Message message=new Message((msg+"-"+System.currentTimeMillis()).getBytes());
        //设置消息的有效期
        message.getMessageProperties().setExpiration("25000");
        template.convertAndSend("","lx-dead-ttl",message);
        return "OK";
    }
}
