package com.qfedu.rabbitmq03;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitMq03Application {

    public static void main(String[] args) {
        SpringApplication.run(RabbitMq03Application.class, args);
    }

}
